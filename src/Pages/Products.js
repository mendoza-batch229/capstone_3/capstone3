import CourseCard from '../Components/ProductCard.js'
import { useEffect, useState } from 'react'
import {Container,  Row} from 'react-bootstrap';


export default function Product(){

	// SHOW ALL PRODUCTS
	const [product, setproduct] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            setproduct(data.map(product => {
	            	return(
	            		<CourseCard key={product._id} productProps={product}/>
	            		)
	            }))

	      });
	}, [])

	return (
		<>
			<h1>Available Product</h1>
			<Container fluid>
			<Row>
		
			{product}
			
		</Row>
			</Container>
		</>
		)

	
}