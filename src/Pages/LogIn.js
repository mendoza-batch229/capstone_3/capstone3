import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate} from "react-router-dom";
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

import './Pages.css';
// import logIn-bg from '../Images/login-design.png';
import LogInbg from '../Images/login-design.png';



export default function LogIn() {

	const navigate = useNavigate();

	//Allow us to consume the User Context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// const userIsAdmin = user.isAdmin;

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
	console.log(email);
    console.log(password);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);


	const retrieveToken = localStorage.getItem("token");
	const [isAdmin, setIsAdmin] = useState("");


  

 

    useEffect(() => {

	    // Validation to enable submit button when all fields are populated and both passwords match
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }
	}, [email, password]);


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
		   headers: {
			  Authorization: `Bearer ${token}`
		   },
		})
		   .then((res) => res.json())
		   .then((data) => {
			  console.log(data);
  
			  setUser({
				 id: data._id,
				 isAdmin: data.isAdmin

			  });
			  if (data.isAdmin) {
				navigate("/adminproduct");
			 } else {
				console.log(data.isAdmin);
				navigate("/products");
			 }
		   });
	 };
  

	function authenticate(e) {
	    e.preventDefault();

	    fetch('http://localhost:4010/users/logIn', {
	    	method: 'POST',
	    	headers: {
	    		'Content-Type': 'application/json'
	    	},
	    	body: JSON.stringify({
	    		email: email,
	    		password: password
	    	})
	    })
    	.then(result => result.json())
    	.then(data => {

    		// It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application
    		console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title: "Authentication Success",
					icon: "success",
				
				})
				// if (userIsAdmin) {
				// 	navigate("/adminproduct");
				//  } else {
				// 	navigate("/products");
				//  }
				
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}

    	})


	    //Set the email of the authenticated user in the local storage
	    /*
			Syntax:
				localStorage.setItem('propertyName', value)
	
	    */
	    // localStorage.setItem('email', email);

    	// Set the global user state to have properties obtained from local storage
        // Though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
        // When states change components are rerendered and the AppNavbar component will be updated based on the user credentials, unlike when using the localStorage where the localStorage does not trigger component rerendering
	    // setUser({
	    // 	email: localStorage.getItem('email')
	    // })


	    // Clear input fields after submission
	    setEmail('');
	    setPassword('');
	}


	// const retrieveUserDetails = (token) =>{
	// 	// The token will be sent as part of the request's header information
	// 	// We put "Bearer" in front of the token to follow implementation standards for JWTs

	// 	fetch('http://localhost:4000/users/details', {
	// 		headers: {
	// 			Authorization: `Bearer ${token}`
	// 		}
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);


	// 	// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
	// 	setUser({
	// 		token: localStorage.getItem('token'),
	// 		id: data._id,
	// 		isAdmin: data.isAdmin

	// 	})
	// })


	

	const [color, changeColor] = useState("peachpuff");
	// document.body.style.backgroundImage = "url('https://media.canva.com/1/image-resize/1/550_343_100_PNG_F/czM6Ly9tZWRpYS1wcml2YXRlLmNhbnZhLmNvbS9wcEZ3TS9NQUZaNy1wcEZ3TS8xL3AucG5n?osig=AAAAAAAAAAAAAAAAAAAAAGQl8zKGDUcoxHyrs2VdVgYDz-l17GL7lj84uweKJvN7&exp=1675828757&x-canva-quality=thumbnail_large&csig=AAAAAAAAAAAAAAAAAAAAAJqkdMFv5BepdE8ZkThxtH41RXran9Nz1ZvgR64WTAkl')";
	// // document.body.style.backgroundRepeat = "no-repeat";
	// document.body.style.backgroundSize = "cover";
	document.body.style.backgroundColor = color;
    return (
		<>
		<Container className='loginConCss'>
	        <h1 id='h1login'>LOGIN</h1>
			<Form className='loginCss' onSubmit={(e) => authenticate(e)}>
		        <Form.Group controlId="userEmail">
		            <Form.Label id='labelloginCss'>Email address</Form.Label>
		            <Form.Control 
		                type="email" 
		                placeholder="Enter email"
		                value={email}
		    			onChange={(e) => setEmail(e.target.value)}
		                required/>
		        </Form.Group>

		        <Form.Group controlId="password">
		            <Form.Label  id='labelloginCss'>Password</Form.Label>
		            <Form.Control 
		                type="password" 
		                placeholder="Password"
		                value={password}
		    			onChange={(e) => setPassword(e.target.value)}
		                required/>
		        </Form.Group>

	            { 
	            	(isActive) ? 
		                <Button id='loginbtnCss submitBtn' className="my-3" variant="success" type="submit">
		                    Login
		                </Button>
		                : 
		                <Button id='loginbtnCss submitBtn' className="my-3" variant="danger" type="submit" disabled>
		                    Login
		                </Button>
	            }
	    </Form>		
		</Container>	
		</>
    )
}
