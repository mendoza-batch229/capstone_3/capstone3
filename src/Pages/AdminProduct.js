import AddProduct from '../Components/AddProduct.js';

import {Container,  Row} from 'react-bootstrap';
import CourseCard from '../Components/ProductCard.js'
import { useEffect, useState } from 'react'


export default function AdminPage(){

	const [product, setproduct] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            setproduct(data.map(product => {
	            	return(
	            		<CourseCard key={product._id} productProps={product}/>
	            		)
	            }))

	      });
	}, [])
	
	return ( 
		<>
    <AddProduct/>
	<h1>Available Product</h1>
			<Container fluid>
			<Row>
		
			{product}
			
		</Row>
			</Container>
	</>
	)

	
}