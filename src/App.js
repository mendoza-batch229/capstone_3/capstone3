import './App.css';

import AppNavbar from './Components/Navbar.js'
// import Products from './Components/Products.js'



import {Container} from 'react-bootstrap/';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Register from './Pages/Register.js';
import LogIn from './Pages/LogIn.js';
import LogOut from './Pages/LogOut.js';
import AdminProduct from './Pages/AdminProduct.js';
import Products from './Pages/Products.js';

import ProductViewEdit from './Components/ProductViewEdit.js'


import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';

function App() {
  const [user, setUser] = useState({

  // token:localStorage.getItem('token') 
  id: null,
  isAdmin: null
})

const unsetUser = () => {
    localStorage.clear();
}

// Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
useEffect(() => {
  console.log(user);
  console.log(localStorage);
})

//  To update the User state upon a page load is initiated and a user already exists.
useEffect(()=>{

  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers:{
      Authorization: `Bearer ${localStorage.getItem("token")}`
    }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data);

    // Set the user states value if the token already exists in the local storage
    if(data._id !== undefined){
      setUser({
        id: data._id,
isAdmin: data.isAdmin
      });
    }
    // set back to the inital state of the user if no token found in the local storage.
    else{
      setUser({
        id: null,
        isAdmin: null
      });
    }
    
  })

}, [])


  return (
    <div className="App">
    <UserProvider value={{user, setUser, unsetUser}}>
    <>
    <Router>
    <AppNavbar/>
    <Container>
      <Routes>
      <Route exact path="/" element={<Products/>}/>
      <Route exact path="/login" element={<LogIn/>}/>
      <Route exact path="/register" element={<Register/>}/>
      <Route exact path="/logout" element={<LogOut/>}/>
      <Route exact path="/adminproduct" element={<AdminProduct/>}/>
      <Route exact path="/products" element={<Products/>}/>

      <Route exact path="/productviewedit/:productId" element={<ProductViewEdit/>}/>
      {/* <Route exact path="*" element={<Error/>}/> */}
      </Routes>
    </Container>
    </Router>  
    </>
    </UserProvider>

    </div>
  );
}

export default App;
