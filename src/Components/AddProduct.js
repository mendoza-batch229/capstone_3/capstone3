import { useEffect, useState } from 'react'
import Swal from 'sweetalert2';
import { useNavigate} from "react-router-dom";
import {Button, Form, Row, Container} from 'react-bootstrap/';

import ProductCard from '../Components/ProductCard.js'

import './Components.css';


export default function Product(){
 
 const navigate = useNavigate();
 // CATCH THE INPUT FROM THE PAGE   
  const [imageURL, setimageURL] = useState('');
  const [name, setname] = useState('');
  const [description, setdescription] = useState('');
  const [price, setprice] = useState('');

 // DISPLAY THE INPUT FROM THE PAGE TO THE CONSOLE 
  console.log(imageURL);
  console.log(name);
  console.log(description);
  console.log(price);

 // FETCHING TO CONNECT FROM DB TO REACT 
//  ADD PRODUCT TO THE DATABASE 
  function insertProduct(e){
	e.preventDefault();
	
			fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`,{
				method: "POST",
				headers:{
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					imageURL: imageURL,
					name: name,
					description: description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if(data){
					Swal.fire({
						title: "You Add Product Successfully",
						icon: "success",
					});
					setimageURL('');
					setname('');
					setdescription('');
					setprice('');
					navigate("/adminproduct");
				}
				else{

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})


		}



  	// USE FOR ENABLING BUTTON 
		const [isActive, setIsActive] = useState(false);
	useEffect(() =>{


			if(imageURL !== '' && name !=='' && description !== '' && price !== ''){
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
	
		}, [imageURL, name, description, price])






		// SHOW ALL PRODUCTS IN ADMIN DASHBOARD
	const [product, setproduct] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            setproduct(data.map(product => {
	            	return(
	            		<ProductCard key={product._id} productProps={product}/>
	            		)
	            }))

	      });
	}, [])

	return (
		<>
 
    <h1>Insert New Item</h1>
	<Form className='addProdCss' onSubmit={e => insertProduct(e)}>
      <Form.Group className="mb-3" controlId="productImage">
        <Form.Label>Product Image</Form.Label>
        <Form.Control 
			type="text" 
			placeholder="Enter url"
			value={imageURL}
            onChange={e => setimageURL(e.target.value)}
            required />

        <Form.Label>Product Name</Form.Label>
        <Form.Control 
			type="text" 
			placeholder="Enter name"
			value={name}
            onChange={e => setname(e.target.value)}
            required />

        <Form.Label>Product Description</Form.Label>
        <Form.Control 
			type="text" 
			placeholder="Enter description"
			value={description}
            onChange={e => setdescription(e.target.value)}
            required />

        <Form.Label>Product Price</Form.Label>
        <Form.Control 
			type="number" 
			placeholder="Enter price"
			value={price}
            onChange={e => setprice(e.target.value)}
            required />

      </Form.Group>
	  
		{
                isActive
                ?
                    <Button id='addProdbtnCss submitBtn' variant="primary" type="submit">
                    Add-Product
                    </Button>
                :
                    <Button id='addProdbtnCss submitBtn' variant="danger" type="submit" disabled>
                    Add-Product
		         </Button>
		}

    </Form>
	
	{/* <Container fluid>
			<Row>
	<Form className='editProdcss'>
	<h1 className=''>Edit Product</h1>
	
	<body id='editProdCss'>{product}</body>
	</Form>
	</Row>
			</Container> */}
			</>
		)

	
}