import { useState, useEffect, useContext,  } from 'react'
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams} from 'react-router-dom'
import { useNavigate} from "react-router-dom";

import Swal from 'sweetalert2'


import UserContext from '../UserContext';

export default function CourseView(){
	const navigate = useNavigate();
    const {user} = useContext(UserContext);


    const { productId } = useParams();

    const [imageURL, setimageURL] = useState('');
    const [name, setname] = useState('');
    const [description, setdescription] = useState('');
    const [price, setprice] = useState('');
	const [isActive, setisActive] = useState('');

	const [quantity, setquantity] = useState('');
	

    useEffect(() => {
		console.log(productId);

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setimageURL(data.imageURL);
			setname(data.name);
			setdescription(data.description);
            setprice(data.price);
			setisActive(data.isActive);


		});

		setquantity(quantity);
		

	}, [productId])

	// BUYING PRODUCT
    const buyProd = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/pOrder`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				imageURL: imageURL,
				pName: name,
				pDesc: description,
				pPrice: price,
				pQuantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Ordered Successfull!",
					icon: 'success',
					text: "We will ship your parcel after we pack it."
				})
				navigate("/products")
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});
	}


	// UPDATING PRODUCT

	const updateProd = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				imageURL: imageURL,
				name: name,
				description: description,
				price: price

				
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: 'success',
					text: "You have successfully enrolled for this course."
				})
				navigate("/adminproduct");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});
	}
	


	// FETCHING TO CONNECT FROM DB TO REACT 
//  ADD TO CART TO THE DATABASE 
	const cartProduct = (productId) => {
	fetch(`${ process.env.REACT_APP_API_URL }/users/add2cart`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Authorization: `Bearer ${ localStorage.getItem('token') }`
		},
		body: JSON.stringify({
			imageURL: imageURL,
			pName: name,
			pDesc: description,
			pPrice: price,
			pQuantity: quantity
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === true){
			Swal.fire({
				title: "Added to cart Successfull!",
				icon: 'success',
				text: "Purchase it now."
			})
			navigate("/products")
		}else{
			Swal.fire({
				title: "Something went wrong!",
				icon: 'error',
				text: "Please try again."
			})
		}

	});
	}


	// ARCHIVING-UNARCHIVING


	const archivingProd = (productId) => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				isActive: isActive	
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: 'success',
					text: "You have successfully enrolled for this course."
				})
				navigate("/adminproduct");
			}else{
				Swal.fire({
					title: "Something went wrong!",
					icon: 'error',
					text: "Please try again."
				})
			}

		});
	}

	


    return(
        <Container className="mt-5">
        <Row>
            <Col lg={{ span: 6, offset: 3 }}>
			{
            (user.isAdmin === true) ?
			<Button id='addProdbtnCss submitBtn' variant="primary" type="submit">
                    Back
                    </Button>
					:
			<Button id='addProdbtnCss submitBtn' variant="primary" type="submit" hidden>
                    Back
                    </Button>
					
			}
                    <Card className="my-3" style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={imageURL} />
                    <Card.Body className="text-center">

                        {
                        (user.token === null || user.isAdmin === false) ?
                        <>
						<Form.Label>{name}</Form.Label>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text> 
						<Card.Subtitle>Quantity:</Card.Subtitle>
						<Form.Control 
							type="number" 
							placeholder=""
							value={quantity}
							onChange={e => setquantity(e.target.value)}
							required />  

                        <Button onClick={() => cartProduct(productId)} variant="primary" block>ADD2CART?</Button>

                        <Button onClick={() => buyProd(productId)} variant="primary" block>BUY NOW?</Button>
                        </>
                        :         
						<> 
						<Form.Label>isActive</Form.Label> 
						<Form.Control 
							type="text" 
							placeholder=""
							value={isActive}
							onChange={e => setisActive(e.target.value)}
							required />
						{/* <Card.Text>{productId}</Card.Text> */}
						<Form.Control 
							type="text" 
							placeholder=""
							value={imageURL}
							onChange={e => setimageURL(e.target.value)}
							required />
						<Form.Label>Product</Form.Label>
						<Form.Control 
							type="text" 
							placeholder=""
							value={name}
							onChange={e => setname(e.target.value)}
							required /> 
						<Form.Label>Product Description</Form.Label>
						<Form.Control 
							type="text" 
							placeholder=""
							value={description}
							onChange={e => setdescription(e.target.value)}
							required />
						<Form.Label>Product Price</Form.Label>
						<Form.Control 
							type="number" 
							placeholder=""
							value={price}
							onChange={e => setprice(e.target.value)}
							required />   
                        <Button onClick={() => updateProd(productId)} variant="primary" block>UPDATE</Button>   
						<Button onClick={() => archivingProd(productId)} variant="primary" block>UN/ARCHIVE</Button> 
						</> 
                        }          
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    </Container>
    )

}
