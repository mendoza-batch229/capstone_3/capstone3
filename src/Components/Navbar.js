import {Container,Navbar, Nav, Image} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';

import {useContext} from 'react';
import UserContext from '../UserContext';

import Icon from '../Images/ALz-Icon.png';


function AppNavbar() {
  //const [user, setUser] = useState(localStorage.getItem('email'));
  const retrieveToken = localStorage.getItem("token");
	
	const {user} = useContext(UserContext);


  return (
    <Navbar  expand="lg">
      <Container className='navBar'>
        <Navbar.Brand as={NavLink} exact to="/products"><Image src={Icon} id='iconNavCss'></Image>𝕺𝖓𝖑𝖎𝖓𝖊 𝕾𝖙𝖔𝖗𝖊</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="">
          
          <Nav.Link as={NavLink} exact to="/" className='mr-auto'></Nav.Link>
         {
          
          (user.token  === null && user.isAdmin === false) ?
          
          <Nav.Link as={NavLink} exact to="/adminproduct"></Nav.Link>
          :
          <Nav.Link as={NavLink} exact to="/adminproduct"></Nav.Link>
         }

         {
            
          (user.token  === null)? 
            <>
            <Nav.Link as={NavLink} exact to="/products" className='mr-auto'>Products</Nav.Link>
            <Nav.Link as={NavLink} exact to="/login">LogIn</Nav.Link>
            <Nav.Link as={NavLink} exact to="/register">Register</Nav.Link> 
            </>
            :
            <>
            <Nav.Link as={NavLink} exact to="/products" className='mr-auto'>Products</Nav.Link>
            <Nav.Link as={NavLink} exact to="/logout">LogOut</Nav.Link> 
            </>
          }
          
         
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;