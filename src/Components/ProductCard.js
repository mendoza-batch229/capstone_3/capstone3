import {Card, Col } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {NavLink} from 'react-router-dom';

import {useContext} from 'react';
import UserContext from '../UserContext';

import './Components.css';

export default function CourseCard({productProps}){
	
	const {user} = useContext(UserContext);
	// Checks to see if the data was successfully passed
	console.log(productProps);
	console.log(typeof productProps);

	// Destructuring the data to avoid dot notation
	const {imageURL, name, description, price, _id} = productProps;


	return(
		<>
		<Col md={4} className='productcardcolCss'>
		 <Card className="my-3 productcardCss" style={{ width: '18rem', height:'100%' }}>
         <Card.Img variant="top" src={imageURL} />
	        <Card.Body className="" >
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
				{
				(user.token  === null)?
				<Link className="btn btn-primary" as={NavLink} exact to="/login">LogIn First</Link>	
				:
				<Link className="btn btn-primary" to={`/productviewedit/${_id}`}>Details</Link>	
				}
	        </Card.Body>        
	    </Card>
		</Col>
        </>
		)
}